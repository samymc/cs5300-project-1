import copy
import functions

def row_has_array(row):
    for value in row:
        if functions.get_type(value) == 'ARRAY':
            return True
    return False

def table_in_1NF(rows):
    for row in rows: # Check the table for arrays
        if row_has_array(row):
            return False
    return True

# Get all arrays from a row and store the columns they were in
def extract_arrays_from_row(row):
    arrays = []
    array_columns = []
    for i in range(len(row)):
        if functions.get_type(row[i]) == 'ARRAY':
            array = row[i].strip('[]').split(';') # Put all values in an array into a list
            arrays.append(array)
            array_columns.append(i) # Store which column had an array
    return {'arrays':arrays,'array_columns':array_columns}

# Returns a list of rows from splitting an array
def convert_array_to_rows(row,array,array_column):
    rows = []
    for value in array:
        new_row = copy.deepcopy(row) # Copy the current row by value
        new_row[array_column] = value # Replace the position in row with new value
        rows.append(new_row) # Append the replaced row to list
    return rows

# Convert a row into a list of rows that have no arrays
def convert_row_to_1NF(row):
    arrays = extract_arrays_from_row(row)
    if len(arrays['arrays']) == 0: # If the row has no arrays return it as a list
        return [row]
    new_rows = []
    if len(arrays['arrays']) == 1: # If a row has one array convert it
        for new_row in convert_array_to_rows(row,arrays['arrays'][0],arrays['array_columns'][0]):
            new_rows.append(new_row)
        return new_rows
    else: # If a row has more than one array convert all of them
        for new_row in convert_array_to_rows(row,arrays['arrays'][0],arrays['array_columns'][0]):
            new_rows.append(new_row)
        for i in range(1,len(arrays['arrays'])):
            for _ in range(len(new_rows)):
                new_rows.extend(convert_array_to_rows(new_rows.pop(0),arrays['arrays'][i],arrays['array_columns'][i]))
        return new_rows

def convert_to_1NF(columns,rows,key):
    if table_in_1NF(rows):
        return rows
    if type(rows[0]) is not list: # If there is only one row
        return convert_row_to_1NF(rows)
    new_rows = []
    for row in rows: # If there are multiple rows
        new_rows.extend(convert_row_to_1NF(row))
    return functions.create_table_on_columns(columns,new_rows,key,columns)
