class Table:
    def __init__(self,columns,column_types,rows,key):
        self.columns = columns
        self.column_types = column_types # Types of each column for SQL query (INT,DATE,VARCHAR)
        self.rows = rows
        self.key = key
        name = ''
        for k in key:
            name += k
        self.name = name
        self.foreign_keys = [] # [[key_column,foreign_table],...]
    
    # Return a SQL query to add the rows of a table
    def export_rows(self):
        export_string = f"INSERT INTO {self.name} VALUES\n"
        for row in self.rows: # Go through each row
            export_string += "("
            for i in range(len(row)): # Add each column in a row
                if self.column_types[i] != 'VARCHAR(255)':
                    export_string += f"{row[i]},"
                else:
                    export_string += f"\"{row[i]}\","
            export_string = export_string[:-1] # Strip last comma
            export_string += "),\n" # Set up for a new row
        export_string = export_string[:-2] + ";" # Strip last comma and newline then end query
        return export_string
    
    # Returns a SQL query to create the table as a string
    def export_table(self):
        export_string = f"CREATE TABLE {self.name} (\n"
        for i in range(len(self.columns)): # Add all columns
            export_string += f"    {self.columns[i]} {self.column_types[i]}{'' if self.columns[i] not in self.key else ' PRIMARY KEY'},\n"
        for key in self.foreign_keys: # Add foreign keys, if any
            export_string += f"    FOREIGN KEY ({key[0]}) REFERENCES {key[1]}({key[0]}),\n"
        export_string = export_string[:-2] + "\n);" # Strip the last comma and newline then end query
        return export_string
