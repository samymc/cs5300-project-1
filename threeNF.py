import oneNF
import twoNF
import functions
import copy

# Find if there are transitive dependencies in the table and return true if there are
def find_transitive_dependencies(columns,dependencies,key):
    for dependency in dependencies:
        dependency_columns = functions.collapse_dependency(dependency) # Group the dependency into a single list
        # If the table's columns have all of the dependency's columns
        if set(dependency_columns).issubset(set(columns)):
            # If the dependency's key and dependents are not in the table's key 
            if not set(dependency['key']).issubset(set(key)) and not set(dependency['dependents']).issubset(set(key)):
                return True
    return False

# Return whether or not the table is in 3NF
def table_in_3NF(columns,rows,dependencies,key):
    if not twoNF.table_in_2NF(columns,rows,dependencies,key): # If it isn't in 2NF it can't be in 3NF
        return False
    if find_transitive_dependencies(columns,dependencies,key):
        return False
    return True

# Convert a given table already in 2NF to 3NF and return the new tables
def convert_from_2NF(columns,rows,dependencies,key):
    newTables = []
    remove_columns = []
    for dependency in dependencies:
        dependency_columns = functions.collapse_dependency(dependency) # Group the dependency into a single list
        # If the table columns has all of the dependency columns
        if set(dependency_columns).issubset(set(columns)):
            # If the dependency is not part of the key
            if not set(dependency['key']).issubset(set(key)):
                # Save the transitive columns to be removed from original table
                remove_columns.extend(dependency['dependents'])
                # Add a new table for the transitive dependency
                newTables.append(functions.create_table_on_columns(columns,rows,dependency['key'],dependency_columns))
    # Remake the original table without transitively dependent columns
    new_columns = copy.deepcopy(columns)
    for column in remove_columns:
        new_columns.remove(column)
    newTables.append(functions.create_table_on_columns(columns,rows,key,new_columns))
    return newTables

# Convert a given table to 3NF and return the new tables
def convert_to_3NF(columns,rows,dependencies,key):
    if table_in_3NF(columns,rows,dependencies,key): # Check if the table is in 3NF and stop if it is
        return [columns,rows,key]
    if not oneNF.table_in_1NF(rows): # If it isn't in 1NF convert to 1NF
        newRows = oneNF.convert_to_1NF(rows)
    else:
        newRows = rows
    if twoNF.table_in_2NF(columns,newRows,dependencies,key): # If the table is in 2NF just run conversion on current table
        return convert_from_2NF(columns,newRows,dependencies,key)
    twoNF_tables = twoNF.convert_to_2NF(columns,rows,dependencies,key) # Convert the table into 2NF tables
    final_tables = []
    for _ in range(len(twoNF_tables)): # Go through each 2NF table and convert to 3NF
        table = twoNF_tables.pop(0) # Remove and save the first item in the list
        if table_in_3NF(table['columns'],table['rows'],dependencies,table['key']): # If the 2NF table is in 3NF skip conversion and add to final list
            final_tables.append(table)
        else: # If the 2NF table isn't in 3NF convert and add to final list
            final_tables.extend(convert_from_2NF(table['columns'],table['rows'],dependencies,table['key']))
    return final_tables
