import oneNF
import functions

# Find if there are partial dependencies in the table and return true if there are
def find_partial_dependencies(columns,key,dependencies):
    # If the key only has one value or is the entire table it can't have partial dependencies
    if len(key) == 1 or len(key) == len(columns):
        return False
    for dependency in dependencies:
        dependency_columns = functions.collapse_dependency(dependency) # Group the dependency into a single list
        # If the table's columns have all of the dependency's columns
        if set(dependency_columns).issubset(set(columns)):
            # If the dependency's key is in the column's key and is not the entire column key
            if set(dependency['key']).issubset(set(key)) and len(dependency['key']) < len(key):
                return True
    return False

# Return whether or not the table is in 2NF
def table_in_2NF(columns,rows,dependencies,key):
    if not oneNF.table_in_1NF(rows): # If it isn't in 1NF it can't be in 2NF
        return False
    if find_partial_dependencies(columns,key,dependencies): # If it has partial dependencies it isn't in 2NF
        return False
    return True

# Group transitive dependencies so they aren't left out when making tables
def associate_extra_dependencies(dependencies):
    keys = []
    key_positions = []
    for i in range(len(dependencies)):
        keys.append(dependencies[i]['key'])
        key_positions.append(i)
    associations = []
    for i in range(len(keys)):
        for j in range(len(dependencies)):
            if i != j and set(keys[i]).issubset(set(dependencies[j]['dependents'])):
                associations.append([dependencies[j]['key'],dependencies[i]['dependents']])
    return associations

# Convert a given table to 2NF and return the new tables
def convert_to_2NF(columns,rows,dependencies,key):
    if table_in_2NF(columns,rows,dependencies,key): # Check if it is already in 2NF and stop if it is
        return [columns,rows,key]
    if not oneNF.table_in_1NF(rows): # If it isn't in 1NF convert to 1NF
        oneNF_Rows = oneNF.convert_to_1NF(rows)
    else:
        oneNF_Rows = rows
    tables = []
    tables.append(functions.create_table_on_columns(columns,oneNF_Rows,key,key)) # Create the connecting table between dependencies
    associations = associate_extra_dependencies(dependencies)
    for dependency in dependencies:
        if set(dependency['key']).issubset(set(key)): # If current dependency's key is a subset of the table key
            if len(dependency['key']) == len(key): # If a dependency contains the whole key
                tables.pop(0) # Remove the incorrect/redundant default table
            table_columns = functions.collapse_dependency(dependency)
            for association in associations: # Add transitive columns to list for new table
                if set(association[0]).issubset(set(table_columns)):
                    table_columns.extend(association[1])
            tables.append(functions.create_table_on_columns(columns,oneNF_Rows,dependency['key'],table_columns)) # Create table based on given columns
    return tables
