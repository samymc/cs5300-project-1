import functions
import oneNF
import twoNF
import threeNF
import BCNF
import table

def get_highest_form(columns,rows,dependencies,key):
    highest_form = 'Not in any normal form'
    if not oneNF.table_in_1NF(rows):
        return highest_form
    highest_form = '1NF'
    
    if not twoNF.table_in_2NF(columns,rows,dependencies,key):
        return highest_form
    highest_form = '2NF'
    
    if not threeNF.table_in_3NF(columns,rows,dependencies,key):
        return highest_form
    highest_form = '3NF'
    
    if not BCNF.table_in_BCNF(columns,rows,dependencies,key):
        return highest_form
    return 'BCNF'

input = functions.get_input()
highest_form = ''
if input['get_current_form'] == '1':
    highest_form = get_highest_form(input['columns'],input['rows'],input['dependencies'],input['key'])
print(f'\nHighest normal form of the table is: {highest_form}')
    
if input['normal_form'] == '1':
    tables = oneNF.convert_to_1NF(input['columns'],input['rows'],input['key'])
elif input['normal_form'] == '2':
    tables = twoNF.convert_to_2NF(input['columns'],input['rows'],input['dependencies'],input['key'])
elif input['normal_form'] == '3':
    tables = threeNF.convert_to_3NF(input['columns'],input['rows'],input['dependencies'],input['key'])
elif input['normal_form'] == 'B':
    tables = BCNF.convert_to_BCNF(input['columns'],input['rows'],input['dependencies'],input['key'])
    
classed_tables = []
if len(tables) != 1:
    for t in tables:
        classed_tables.append(table.Table(t['columns'],functions.get_row_types(t['rows'][0]),t['rows'],t['key']))
else:
    classed_tables.append(table.Table(tables['columns'],functions.get_row_types(tables['rows'][0]),tables['rows'],tables['key']))
    
functions.set_foreign_keys(classed_tables)
print('')
for t in classed_tables:
    print(t.export_table())
print('')
for t in classed_tables:
    print(t.export_rows())
