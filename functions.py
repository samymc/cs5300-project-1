def get_input():
    filename = input("Enter filename for table input: ")
    columns = []
    rows = []
    
    # Open the file and get all lines from CSV
    with open(filename) as file: 
        lines = file.readlines()
        newLines = []
        for line in lines:
            newLines.append(line[:-1])
        columns = newLines[0].split(',')
        for line in newLines[1:]:
            row = line.split(',')
            rows.append(row)
    
    # Get functional dependencies from user
    dependencies = []
    print('Enter functional dependencies. Enter "exit" to stop') 
    func_depend = input('>>> ')
    while func_depend != 'exit':
        dependency = func_depend.replace(' ','').split('->') # Strip all whitespace and split into a list on ->
        if len(dependency) == 1: # If the list had no -> it is invalid, then don't add to dependencies
            print('No/Invalid functional dependency entered, please enter a functional dependency of the form "[Determinant 1], ... -> [Dependent 1], [Dependent 2], ..."')
        else:
            dependency_dict = {'key':[],'dependents':[]}
            dependency_dict['key'] = dependency[0].split(',')
            dependency_dict['dependents'] = dependency[1].split(',')
            dependencies.append(dependency_dict)
        func_depend = input('>>> ')
    
    # Get choice for target normal form
    normal_form = input('Choose the highest normal form to reach (1: 1NF, 2: 2NF, 3: 3NF, B: BCNF): ')
    if normal_form == '':
        print('No target normal form entered,defaulting to 1NF')
        normal_form = '1'
        
    # Get choice for finding current form
    get_current_form = input('Find the highest normal form of the input table? (1: Yes, 2: No): ')
    if get_current_form == '':
        print('No choice for getting the normal form of the table entered, defaulting to "No"')
        get_current_form = 2
        
    # Get input for table key
    key = input('Key (can be composite): ').replace(' ','').split(',')
    while key[0] == '':
        key = input('No key entered, please enter a key: ').replace(' ','').split(',')
    
    return {'columns':columns,'rows':rows,'dependencies':dependencies,'normal_form':normal_form,'get_current_form':get_current_form,'key':key}

# Return the type of a single value in a row
def get_type(value):
    if '[' in value: # If the value contains a bracket,only used for 1NF checks
        return 'ARRAY'
    if value.isdigit(): # If the value has only digits as characters
        return 'INT'
    if len(value.split('/')) == 3: # If the value is in a date format
        return 'DATE'
    else: # Everything else assume a string
        return 'VARCHAR(255)'

# Return the types of a row of values
def get_row_types(row):
    row_types = []
    for value in row:
        row_types.append(get_type(value))
    return row_types

def collapse_dependency(dependency):
    collapsed_dependency = []
    collapsed_dependency.extend(dependency['key'])
    collapsed_dependency.extend(dependency['dependents'])
    return collapsed_dependency

# Create a table based on existing data with new key and columns
def create_table_on_columns(columns,rows,key,table_columns):
    table = {'columns':[],'rows':[],'key':[]}
    column_numbers = []
    for column in table_columns: # Add the columns and key
        table['columns'].append(column)
        if column in key:
            table['key'].append(column)
        column_numbers.append(columns.index(column))
        
    for row in rows: # Add the rows from added columns
        table_row = []
        for column_number in column_numbers:
            table_row.append(row[column_number])
        table['rows'].append(table_row)
    return table

def set_foreign_keys(tables):
    for table in tables:
        for column in table.columns:
            for table_compare in tables:
                # If not comparing to itself and the column is in the comparison's key
                if table_compare != table and column in table_compare.key:
                    table.foreign_keys.append([column,table_compare.name])
    return