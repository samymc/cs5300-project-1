import functions
import oneNF
import threeNF
import copy

def table_in_BCNF(columns,rows,dependencies,key):
    if not threeNF.table_in_3NF(columns,rows,dependencies,key):
        return False
    for dependency in dependencies:
        dependency_columns = functions.collapse_dependency(dependency) # Group the dependency into a single list
        # If the table's columns have all of the dependency's columns
        if set(dependency_columns).issubset(set(columns)):
            # If the dependency's key is not in the key while the dependents are 
            if not set(dependency['key']).issubset(set(key)) and set(dependency['dependents']).issubset(set(key)):
                return False
    return True

def convert_from_3NF(columns,rows,dependencies,key):
    tables = []
    new_columns = copy.deepcopy(columns)
    for dependency in dependencies:
        dependency_columns = functions.collapse_dependency(dependency) # Group the dependency into a single list
        # If the table's columns have all of the dependency's columns
        if set(dependency_columns).issubset(set(columns)):
            # If the dependency's key is not in the key while the dependents are 
            if not set(dependency['key']).issubset(set(key)) and set(dependency['dependents']).issubset(set(key)):
                # Split off reverse dependency into table
                tables.append(functions.create_table_on_columns(columns,rows,dependency['key'],dependency_columns))
                for column in dependency['dependents']:
                    new_columns.remove(column)
    # Add reverse dependency key and old key minus reverse dependents to new table as all keys
    tables.append(functions.create_table_on_columns(columns,rows,new_columns,new_columns))
    return tables

def convert_to_BCNF(columns,rows,dependencies,key):
    if table_in_BCNF(columns,rows,dependencies,key): # Check if the table is in 3NF and stop if it is
            return [columns,rows,key]
    if not oneNF.table_in_1NF(rows): # If it isn't in 1NF convert to 1NF
        newRows = oneNF.convert_to_1NF(rows)
    else:
        newRows = rows
    if threeNF.table_in_3NF(columns,newRows,dependencies,key): # If the table is in 3NF just run conversion on current table
        return convert_from_3NF(columns,newRows,dependencies,key)
    threeNF_tables = threeNF.convert_to_3NF(columns,rows,dependencies,key) # Convert the table into 2NF tables
    final_tables = []
    for _ in range(len(threeNF_tables)): # Go through each 3NF table and convert to BCNF
        table = threeNF_tables.pop(0) # Remove and save the first item in the list
        if table_in_BCNF(table['columns'],table['rows'],dependencies,table['key']): # If the 3NF table is in BCNF skip conversion and add to final list
            final_tables.append(table)
        else: # If the 3NF table isn't in BCNF convert and add to final list
            final_tables.extend(convert_from_3NF(table['columns'],table['rows'],dependencies,table['key']))
    return final_tables